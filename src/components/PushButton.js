import React from 'react';
import { Button } from 'react-bootstrap';

const PushButton = ({ value, onClick, bsStyle }) => (
  <Button bsStyle={bsStyle} type="button" className="number" onClick={onClick}>
    {value}
  </Button>
);

export default PushButton;
