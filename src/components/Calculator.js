import React from 'react';
import { round } from 'lodash';

import PushButton from './PushButton';


function pushDigit(num, digit, decimal) {
  if (decimal == null) {
    return num * 10 + digit;
  }
  const temp = (0.1 ** decimal);
  return round(num + temp * digit, decimal);
}

function calculate(a, b, op) {
  switch (op) {
    case '+': return a + b;
    case '-': return a - b;
    case '*': return a * b;
    case '/': return a / b;
    default: return 0;
  }
}

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stage: 1,
      valueA: 0,
      valueB: null,
      result: null,
      operantion: null,
      decimal: null,
    };
  }

  handleNumber(i) {
    const { stage } = this.state;
    switch (stage) {
      case 1:
        this.setState(prevState => ({
          valueA: pushDigit(prevState.valueA, i, prevState.decimal),
          decimal: (prevState.decimal) ? prevState.decimal + 1 : null,
        }));
        break;
      case 2:
        this.setState({
          valueB: i,
          stage: 3,
        });
        break;
      case 3:
        this.setState(prevState => ({
          valueB: pushDigit(prevState.valueB, i, prevState.decimal),
          decimal: (prevState.decimal) ? prevState.decimal + 1 : null,
        }));
        break;
      case 4:
        this.setState({
          valueA: i,
          stage: 1,
        });
        break;
      default: // case that should not be used
    }
  }

  handleOperation(i) {
    const { stage } = this.state;
    switch (stage) {
      case 1:
        this.setState({
          operantion: i,
          stage: 2,
          valueB: 0,
          decimal: null,
        });
        break;
      case 2:
        this.setState({
          operantion: i,
          stage: 2,
        });
        break;
      case 3:
        this.setState(prevState => ({
          valueA: calculate(prevState.valueA, prevState.valueB, prevState.operantion),
          stage: 2,
          operantion: i,
          decimal: null,
        }));
        break;
      case 4:
        this.setState(prevState => ({
          valueA: prevState.result,
          stage: 2,
          operantion: i,
          decimal: null,
        }));
        break;
      default: // case that should not be used
    }
  }

  handleEqual() {
    const { stage } = this.state;
    switch (stage) {
      case 3:
        this.setState(prevState => ({
          result: calculate(prevState.valueA, prevState.valueB, prevState.operantion),
          stage: 4,
          decimal: null,
        }));
        break;
      default: // case that should not be used
    }
  }

  // TODO : if decimal is already pressed
  handleDecimal() {
    const { stage } = this.state;
    switch (stage) {
      case 1:
      case 3:
        this.setState({
          decimal: 1,
        });
        break;
      default: // case that should not be used
    }
  }

  renderNumber(i) {
    return (<PushButton bsStyle="success" value={i} onClick={() => this.handleNumber(i)} />);
  }

  renderOperation(i) {
    return (<PushButton bsStyle="info" value={i} onClick={() => this.handleOperation(i)} />);
  }

  renderEqual(i) {
    return (<PushButton bsStyle="danger" value={i} onClick={() => this.handleEqual()} />);
  }

  renderDecimal(i) {
    return (<PushButton bsStyle="info" value={i} onClick={() => this.handleDecimal()} />);
  }

  render() {
    const {
      stage,
      valueA,
      valueB,
      result,
    } = this.state;
    const output = (() => {
      switch (stage) {
        case 1: return valueA;
        case 2: return valueA;
        case 3: return valueB;
        case 4: return result;
        default: return 0;
      }
    })();

    return (
      <div>
        <div>{output}</div>
        {/*        <div>st->{this.state.stage}</div>
        <div>vA->{this.state.valueA}</div>
        <div>op->{this.state.operantion}</div>
        <div>vB->{this.state.valueB}</div>
        <div>de->{this.state.decimal}</div>
        <div>re->{this.state.result}</div> */}
        <table id="board">
          <tr>
            <td>{this.renderNumber(1)}</td>
            <td>{this.renderNumber(2)}</td>
            <td>{this.renderNumber(3)}</td>
            <td>{this.renderOperation('+')}</td>
          </tr>
          <tr>
            <td>{this.renderNumber(4)}</td>
            <td>{this.renderNumber(5)}</td>
            <td>{this.renderNumber(6)}</td>
            <td>{this.renderOperation('-')}</td>
          </tr>
          <tr>
            <td>{this.renderNumber(7)}</td>
            <td>{this.renderNumber(8)}</td>
            <td>{this.renderNumber(9)}</td>
            <td>{this.renderOperation('*')}</td>
          </tr>
          <tr>
            <td>{this.renderNumber(0)}</td>
            <td>{this.renderDecimal('.')}</td>
            <td>{this.renderOperation('/')}</td>
            <td>{this.renderEqual('=')}</td>
          </tr>
        </table>
      </div>
    );
  }
}

export default Calculator;
