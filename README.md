# Calculator App
### by gnkl

A minimal calculator supporting the most common operations.


# Version2
The version2 uses stages-logic to support the different operations.
The logic goes like this :

### Stage 1
Input of the 1st number (1st number is 0 at the beginning)
Output is the 1st number of choice
Operation is null, 2nd value is null, result is null

### Stage 2
Just selected an operantion
Output is the 1st number still
2nd value is 0, result is null

### Stage 3
Started typing the 2nd number
Result is null

### Stage 4
Button of equal sign is pressed
Result has the value and is at the output screen
